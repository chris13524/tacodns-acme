FROM node:12-alpine
RUN apk add dumb-init

WORKDIR /home/node/app
COPY . .
RUN npm install

ENV PORT=80 NODE_ENV=production
CMD ["dumb-init", "--", "node", "index.js"]
EXPOSE 80
