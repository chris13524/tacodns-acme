# TacoDNS ACME

This is a sidecar for [TacoDNS](https://gitlab.com/chris13524/tacodns)
which implements TRPP to enable ACME clients to set DNS challenges.

Compatible with LEGO ACME client's [HTTP Request provider](https://go-acme.github.io/lego/dns/httpreq/).

## Usage

Example docker-compose.yml for use with Traefik:
```
version: "3.7"
services:

  traefik:
    image: traefik:2.0
    restart: always
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./acme/:/acme/
    ports:
      - "443:443"
    command:
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.docker.network=tacodns_default"
      - "--entrypoints.websecure.address=:443"
      - "--certificatesresolvers.default.acme.dnschallenge.provider=httpreq"
      - "--certificatesresolvers.default.acme.dnschallenge.delayBeforeCheck=1"
      - "--certificatesresolvers.default.acme.storage=/acme/acme.json"
    environment:
      HTTPREQ_ENDPOINT: http://tacodns-acme/set
      HTTPREQ_USERNAME: user
      HTTPREQ_PASSWORD: $ACME_PASS

  tacodns-acme:
    restart: always
    image: registry.gitlab.com/chris13524/tacodns-acme
    environment:
      USERNAME: user
      PASSWORD: $ACME_PASS
      
  tacodns:
    restart: always
    image: registry.gitlab.com/chris13524/tacodns
    ports:
      - ...
    command: --config-env TACODNS --verbose
    environment:
      TACODNS: |
        ttl: 5m
        zones:

          "_acme-challenge.**":
            TRPP: http://tacodns-acme/lookup
            
          # ... the rest of your DNS
```
