const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

const challenges = {};

const username = process.env.USERNAME || "";
const password = process.env.PASSWORD;
if (password == null) throw new Error("missing PASSWORD env variable");

const router = express.Router();
router.use(express.json());
router.use(require("express-basic-auth")({
	users: {[username]: password}
}));
router.post("/present", (req, res) => {
	const fqdn = req.body.fqdn;
	if (fqdn == null) {
		console.error("400 missing fqdn field:", req.body);
		res.status(400);
		res.json({error: "missing fqdn field"});
		return;
	}
	
	const value = req.body.value;
	if (value == null) {
		console.error("400 missing value field:", req.body);
		res.status(400);
		res.json({error: "missing value field"});
		return;
	}
	
	challenges[fqdn.toLowerCase()] = value;
	res.sendStatus(204);
});
router.post("/cleanup", (req, res) => {
	const fqdn = req.body.fqdn;
	if (fqdn == null) {
		console.error("400 missing fqdn field:", req.body);
		res.status(400);
		res.json({error: "missing fqdn field"});
		return;
	}
	
	delete challenges[fqdn.toLowerCase()];
	res.sendStatus(204);
});
app.use("/set", router);

app.get("/lookup", (req, res) => {
	const name = req.query.name;
	if (name == null) {
		console.error("400 missing name query param:", req.query);
		res.status(400);
		res.json({error: "missing name query param"});
		return;
	}
	const name_normalized = name.toLowerCase();
	if (!name_normalized.startsWith("_acme-challenge")) {
		console.error("400 cannot lookup name that doesn't start with `_acme-challenge`:", name_normalized);
		res.status(400);
		res.json({error: "cannot lookup name that doesn't start with `_acme-challenge`"});
		return;
	}

	const type = req.query.type;
	if (type == null) {
		console.error("400 missing type query param:", req.query);
		res.status(400);
		res.json({error: "missing type query param"});
		return;
	}
	if (type !== "TXT") {
		console.error("400 cannot lookup record that isn't of type TXT:", type);
		res.status(400);
		res.json({error: "cannot lookup record that isn't of type TXT"});
		return;
	}
	
	const challenge = challenges[name_normalized + "."];
	if (challenge == null) {
		console.error("404 challenge not found");
		res.status(404);
		res.json({error: "challenge not found"});
		return;
	}

	res.json([{rec: challenge}]);
});

app.listen(port);
